<?php

namespace Drupal\user_cancel_entity_queue\Form;

use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Configure User cancel entity queue settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'user_cancel_entity_queue_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['user_cancel_entity_queue.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('user_cancel_entity_queue.settings');
    $enabled_entity_types = $config->get('enabled_entity_types') ?: [];


    $form['intro'] = [
      '#markup' => '<p>You can select on which entities the deletion queue should work on once a user gets canceled</p>',
    ];

    $form['entities'] = [
      '#type' => 'details',
      '#title' => $this->t('enabled Entities')
    ];

    $entity_type_definitions = \Drupal::entityTypeManager()->getDefinitions();
    // Only use Content entity types.
    $entity_type_definitions = array_filter($entity_type_definitions, function($entity_type) {
      return $entity_type instanceof ContentEntityType;
    });

    foreach ($entity_type_definitions as $name => $entity_type_plugin) {
      $form['entities']['entity_type_'. $name] = [
        '#type' => 'checkbox',
        '#title' => $entity_type_plugin->getLabel(),
        '#default_value' => in_array($name, $enabled_entity_types)
      ];
    }

    $form['reassignment_uid'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#title' => $this->t('New Author for reassignment method'),
      '#description' => $this->t('When the user selects the "Delete the account and assign all its content to another user via queue." method, the entities will be assigned to this user.'),
      '#default_value' => is_numeric($config->get('reassignment_uid')) ? User::load($config->get('reassignment_uid')) : NULL
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('user_cancel_entity_queue.settings');
    $entities = [];

    // Get all values from form for saving.
    $form_state->cleanValues();
    $values = $form_state->getValues();

    foreach ($values as $key => $value) {
      if (strpos($key, 'entity_type_') === 0) {
        if ($value) {
          $entities[] = str_replace('entity_type_', '', $key);
        }
      }
      else {
        $config->set($key, $value);
      }
    }

    $config->set('enabled_entity_types', $entities);
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
