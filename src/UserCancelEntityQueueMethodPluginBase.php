<?php

namespace Drupal\user_cancel_entity_queue;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for user_cancel_entity_queue_method plugins.
 */
abstract class UserCancelEntityQueueMethodPluginBase extends PluginBase implements UserCancelEntityQueueMethodInterface {

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

}
