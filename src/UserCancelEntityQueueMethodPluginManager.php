<?php

namespace Drupal\user_cancel_entity_queue;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * UserCancelEntityQueueMethod plugin manager.
 */
class UserCancelEntityQueueMethodPluginManager extends DefaultPluginManager {

  /**
   * Constructs UserCancelEntityQueueMethodPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/UserCancelEntityQueueMethod',
      $namespaces,
      $module_handler,
      'Drupal\user_cancel_entity_queue\UserCancelEntityQueueMethodInterface',
      'Drupal\user_cancel_entity_queue\Annotation\UserCancelEntityQueueMethod'
    );
    $this->alterInfo('user_cancel_entity_queue_method_info');
    $this->setCacheBackend($cache_backend, 'user_cancel_entity_queue_method_plugins');
  }

}
