<?php

namespace Drupal\user_cancel_entity_queue;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface for user_cancel_entity_queue_method plugins.
 */
interface UserCancelEntityQueueMethodInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * Performs the queue item action on the user's entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @param array $additional_data
   *
   * @return bool
   *   Whether the action was successful.
   */
  public function performAction(EntityInterface $entity, array $additional_data): bool;
}
