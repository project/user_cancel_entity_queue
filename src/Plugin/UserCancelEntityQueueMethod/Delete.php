<?php

namespace Drupal\user_cancel_entity_queue\Plugin\UserCancelEntityQueueMethod;

use Drupal\Core\Entity\EntityInterface;
use Drupal\user_cancel_entity_queue\Annotation\UserCancelEntityQueueMethod;
use Drupal\user_cancel_entity_queue\UserCancelEntityQueueMethodPluginBase;

/**
 * Plugin implementation of the user_cancel_entity_queue_method.
 *
 * @UserCancelEntityQueueMethod(
 *   id = "user_cancel_entity_queue_delete",
 *   label = @Translation("Delete the account and all its content via queue. This action cannot be undone."),
 *   description = @Translation("The account and all of its content will be deleted asynchronously via a cron queue.")
 * )
 */
class Delete extends UserCancelEntityQueueMethodPluginBase {

  /**
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function performAction(EntityInterface $entity, array $additional_data): bool {
    $entity->delete();
    return true;
  }

}
