<?php

namespace Drupal\user_cancel_entity_queue\Plugin\UserCancelEntityQueueMethod;

use Drupal\Core\Entity\EntityInterface;
use Drupal\user_cancel_entity_queue\Annotation\UserCancelEntityQueueMethod;
use Drupal\user_cancel_entity_queue\UserCancelEntityQueueMethodPluginBase;

/**
 * Plugin implementation of the user_cancel_entity_queue_method.
 *
 * @UserCancelEntityQueueMethod(
 *   id = "user_cancel_entity_queue_assign",
 *   label = @Translation("Delete the account and assign all its content to another user via queue. This action cannot be undone."),
 *   description = @Translation("The account will be deleted and all of its content will be owned by another user via a cron queue.")
 * )
 */
class Reassign extends UserCancelEntityQueueMethodPluginBase {

  /**
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function performAction(EntityInterface $entity, array $additional_data): bool {
    $targeted_uid = $additional_data['reassignment_uid'] ?: 0;
    $entity->set('uid', $targeted_uid);
    $entity->save();
    return true;
  }

}
