<?php

namespace Drupal\user_cancel_entity_queue\Plugin\QueueWorker;

use Drupal\Core\Annotation\QueueWorker;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user_cancel_entity_queue\UserCancelEntityQueueMethodPluginManager;

/**
 * Processes tasks for user_cancel_entity_queue module.
 *
 * @QueueWorker(
 *   id = "user_cancel_entity_queue",
 *   title = @Translation("user_cancel_entity_queue"),
 * )
 */
class UserCancelEntityQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The queue worker plugin manager.
   *
   * @var \Drupal\user_cancel_entity_queue\UserCancelEntityQueueMethodPluginManager
   */
  protected $pluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.user_cancel_entity_queue_method'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, UserCancelEntityQueueMethodPluginManager $plugin_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $storage = $this->entityTypeManager->getStorage($data['entity_type_id']);

    /* @var \Drupal\user_cancel_entity_queue\UserCancelEntityQueueMethodInterface $method_instance */
    $method_instance = $this->pluginManager->createInstance($data['method']);

    // Run workers.
    foreach ($data['entity_ids'] as $entity_id) {
      $entity = $storage->load($entity_id);
      if ($entity instanceof EntityInterface) {
        $method_instance->performAction($entity, $data);
      }
    }
  }

}
