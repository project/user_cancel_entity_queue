<?php

namespace Drupal\user_cancel_entity_queue\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines user_cancel_entity_queue_method annotation object.
 *
 * @Annotation
 */
class UserCancelEntityQueueMethod extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
